import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { DnaModule } from './api/api.module';

@Module({
  imports: [TypeOrmModule.forRoot(), DnaModule],
})
export class AppModule {}
