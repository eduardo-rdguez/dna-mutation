import { Controller, Get } from '@nestjs/common';
import { Statistics } from '../interface/statistics.interface';
import { StatisticsService } from '../service/statistics.service';

@Controller('/stats')
export class StatisticsController {
  constructor(private readonly statisticsService: StatisticsService) {}

  @Get()
  async find(): Promise<Statistics> {
    return await this.statisticsService.find();
  }
}
