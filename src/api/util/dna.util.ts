import { ApiConstants } from '../constant/api.constants';

export class DnaUtil {
  static async validateDna(dna: string[]): Promise<boolean> {
    return dna.every(
      (sequence) => sequence.match(ApiConstants.VALID_SEQNC_REGEX) == null,
    );
  }

  static detectMutation(sequence: string[]): boolean {
    return ApiConstants.MUTATION_SEQNC_REGEX.test(sequence.join(''));
  }
}
