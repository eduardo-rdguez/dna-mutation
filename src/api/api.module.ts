import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DnaController } from './controller/dna.controller';
import { DnaService } from './service/dna.service';
import { DnaEntity } from './entity/dna.entity';
import { StatisticsController } from './controller/statistics.controller';
import { StatisticsService } from './service/statistics.service';
import { MutationService } from './service/mutation.service';

@Module({
  imports: [TypeOrmModule.forFeature([DnaEntity])],
  controllers: [DnaController, StatisticsController],
  providers: [DnaService, StatisticsService, MutationService],
})
export class DnaModule {}
