import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DnaEntity } from '../entity/dna.entity';
import { DnaUtil } from '../util/dna.util';
import { MutationService } from './mutation.service';

@Injectable()
export class DnaService {
  constructor(
    @InjectRepository(DnaEntity)
    private dnaRespository: Repository<DnaEntity>,
    private mutationService: MutationService,
  ) {}

  async create(dna: string[]): Promise<boolean> {
    const existingDna = await this.findOne(dna);
    const acceptedDna = await DnaUtil.validateDna(dna);

    if (!existingDna && acceptedDna) {
      const dnaEntity = this.createDnaEntity(dna);
      this.dnaRespository.save(dnaEntity);
      return dnaEntity.hasMutation;
    }

    return false;
  }

  findOne(dna: string[]): Promise<DnaEntity> {
    const sequence = dna.join('');

    return this.dnaRespository.findOne({
      where: { sequence: sequence },
    });
  }

  createDnaEntity(dna: string[]): DnaEntity {
    const dnaEntity = new DnaEntity();

    dnaEntity.hasMutation = this.mutationService.hasMutation(dna);
    dnaEntity.sequence = dna.join('');

    return dnaEntity;
  }

  async findAll(): Promise<DnaEntity[]> {
    return await this.dnaRespository.find();
  }
}
