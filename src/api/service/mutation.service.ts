import { Injectable, Logger } from '@nestjs/common';
import { DnaUtil } from '../util/dna.util';

@Injectable()
export class MutationService {
  private readonly logger = new Logger(MutationService.name);
  private counter: number;

  hasMutation(dna: string[]): boolean {
    const numRows = dna.length;
    const numCols = dna[0].length;
    this.counter = 0;

    try {
      this.diagonalSearch(dna, numRows, numCols);
      this.verticalSearch(dna);
      this.horizontalSearch(dna);

      return this.counter > 1;
    } catch (e) {
      this.logger.log(`An error occurred while detecting the mutation: ${e}`);
      return false;
    }
  }

  diagonalSearch(dna: string[], numRows: number, numCols: number) {
    const maxLenght = Math.max(numRows, numCols);

    for (let y = 0; y <= 2 * (maxLenght - 1); ++y) {
      const topToBottom = [];
      const bottomToTop = [];

      for (let x = numRows - 1; x >= 0; --x) {
        const z = y - x;
        const w = y - (numRows - x);
        if (z >= 0 && z < numCols) {
          topToBottom.push(dna[x][z]);
        }
        if (w >= 0 && w < numCols) {
          bottomToTop.push(dna[x][w]);
        }
      }

      const hasMutation =
        DnaUtil.detectMutation(bottomToTop) ||
        DnaUtil.detectMutation(topToBottom);
      if (hasMutation) {
        this.counter++;
      }
    }
  }

  verticalSearch(dna: string[]) {
    return dna.some((_, i) => {
      const sequence = dna.map((element) => element[i]);

      if (DnaUtil.detectMutation(sequence)) {
        this.counter++;
      }
    });
  }

  horizontalSearch(dna: string[]) {
    return dna.some((sequence) => {
      if (DnaUtil.detectMutation(sequence.split(''))) {
        this.counter++;
      }
    });
  }
}
