export class ApiConstants {
  static readonly MUTATION_SEQNC_REGEX = /(\w)\1{3,}/;

  static readonly VALID_SEQNC_REGEX = /[^ATCG]/;
}
