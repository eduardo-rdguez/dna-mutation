import { Test, TestingModule } from '@nestjs/testing';
import { DnaService } from '../api/service/dna.service';
import { DnaController } from '../api/controller/dna.controller';
import { TypeOrmSqliteTestingModule } from './util/db.util';
import { MutationService } from '../api/service/mutation.service';

describe('DnaController', () => {
  let controller: DnaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [...TypeOrmSqliteTestingModule()],
      controllers: [DnaController],
      providers: [DnaService, MutationService],
    }).compile();

    controller = module.get<DnaController>(DnaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
