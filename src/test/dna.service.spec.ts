import { Test, TestingModule } from '@nestjs/testing';
import { DnaService } from '../api/service/dna.service';
import { MutationService } from '../api/service/mutation.service';
import { TypeOrmSqliteTestingModule } from './util/db.util';

describe('DnaService', () => {
  let service: DnaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [...TypeOrmSqliteTestingModule()],
      providers: [DnaService, MutationService],
    }).compile();

    service = module.get<DnaService>(DnaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
